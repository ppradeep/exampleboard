#!/bin/bash
set -e

USAGE_STRING="$0 <Boardname>"

if [ "$#" -ne 1 ]
then
  echo "Invalid usage!"
  echo "$USAGE_STRING"
fi


BOARDNAME_ORIGINAL=$1
BOARDNAME_LOWERCASE=${BOARDNAME_ORIGINAL,,}
BOARDNAME_UPPERCASE=${BOARDNAME_ORIGINAL^^}
PLUGIN_REPO_PATH=$(readlink -f $(dirname $BASH_SOURCE))

if [[ "$BOARDNAME_ORIGINAL" =~ [^a-zA-Z0-9] ]]
then
  echo "ERROR: The board name should only contain alphanumeric characters"
  exit 1
fi


echo "Moving header files from include/exampleboard to include/${BOARDNAME_LOWERCASE}"
git mv include/exampleboard include/${BOARDNAME_LOWERCASE}
sed -i "s/EXAMPLEBOARD/${BOARDNAME_UPPERCASE}/g" $(find ${PLUGIN_REPO_PATH}/include -type f)

echo -e "\nChanging namespace from 'exampleboard' to '${BOARDNAME_LOWERCASE}'"
sed -i "s/exampleboard/${BOARDNAME_LOWERCASE}/g" $(find ${PLUGIN_REPO_PATH}/include ${PLUGIN_REPO_PATH}/src -type f)
sed -i "s/ExampleBoard/${BOARDNAME_ORIGINAL}/g" $(find ${PLUGIN_REPO_PATH}/src -type f)
git add $(find ${PLUGIN_REPO_PATH}/include ${PLUGIN_REPO_PATH}/src -type f)

echo -e "\nUpdating herd.yml"
sed -i "s/exampleboard/${BOARDNAME_LOWERCASE}/g" herd.yml
sed -i "s/ExampleBoard/${BOARDNAME_ORIGINAL}/g" herd.yml
git add herd.yml

echo -e "\nUpdating .gitlab-ci.yml"
sed -i "s/exampleboard/${BOARDNAME_LOWERCASE}/g" .gitlab-ci.yml
git add .gitlab-ci.yml

echo -e "\nUpdating CMakeLists.txt"
sed -i "s/exampleboard/${BOARDNAME_LOWERCASE}/g" CMakeLists.txt
sed -i "s/EXAMPLEBOARD/${BOARDNAME_UPPERCASE}/g" CMakeLists.txt
git add CMakeLists.txt

echo -e "\nCommitting the changes"
git commit -m "Board name changed to ${BOARDNAME_ORIGINAL}"

echo -e "\nUpdating URL of git remote 'origin'"
git remote set-url origin ssh://git@gitlab.cern.ch:7999/cms-cactus/phase2/software/plugins/${BOARDNAME_LOWERCASE}.git