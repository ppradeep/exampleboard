#include "exampleboard/swatch/TCDS.hpp"


namespace exampleboard {
namespace swatch {

using namespace ::swatch;

TCDS::TCDS() :
  TTCInterface()
// FIXME: Register metrics for monitoring data here
{
}


TCDS::~TCDS()
{
}


bool TCDS::checkPresence() const
{
  return true;
}


void TCDS::retrievePropertyValues()
{
}


void TCDS::retrieveMetricValues()
{
  // FIXME: Read monitoring data from FPGA, and set values of corresponding metrics
  //        Including metrics from base class: mMetricBunchCounter, mMetricL1ACounter, mMetricOrbitCounter, mMetricIsClock40Locked, mMetricHasClock40Stopped, mMetricIsBC0Locked
  // setMetric(mMetricBunchCounter, VALUE);
  // setMetric(mMetricL1ACounter, VALUE);
  // setMetric(mMetricOrbitCounter, VALUE);
  // setMetric(mMetricIsClock40Locked, VALUE);
  // setMetric(mMetricHasClock40Stopped, VALUE);
  // setMetric(mMetricIsBC0Locked, VALUE);
}

} // namespace swatch
} // namespace exampleboard
