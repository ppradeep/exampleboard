#include "exampleboard/swatch/OutputPort.hpp"


namespace exampleboard {
namespace swatch {

using namespace ::swatch;

OutputPort::OutputPort(const size_t i, const phase2::ChannelID& connectedOpticsChannel) :
  phase2::OutputPort(i, createId(i), connectedOpticsChannel)
// FIXME: Register metrics for monitoring data here
{
}


OutputPort::~OutputPort()
{
}


std::string OutputPort::createId(const size_t i)
{
  std::ostringstream lOSS;
  lOSS << "Tx" << std::setw(2) << std::setfill('0') << i;
  return lOSS.str();
}


bool OutputPort::checkPresence() const
{
  // FIXME: Return true if link firmware has been instantiated for this output channel
  //        in the currently-loaded FW; return false otherwise.
  return true;
}


void OutputPort::retrievePropertyValues()
{
}


void OutputPort::retrieveMetricValues()
{
  // FIXME: Read monitoring data from FPGA, and set values of corresponding metrics
  //        Including metrics from base class: mMetricIsOperating
  // setMetric(mMetricIsOperating, VALUE);
}

} // namespace swatch
} // namespace exampleboard
