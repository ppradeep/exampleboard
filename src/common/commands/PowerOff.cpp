#include <iostream>

#include "exampleboard/swatch/commands/PowerOff.hpp"

#include "swatch/phase2/Processor.hpp"


namespace exampleboard {
namespace swatch {
namespace commands {

using namespace ::swatch;


PowerOff::PowerOff(const std::string& aId, action::ActionableObject& aActionable) :
  Command(aId, "Power off", aActionable, std::string())
{
}


PowerOff::~PowerOff()
{
}


action::Command::State PowerOff::code(const core::ParameterSet& params)
{
  // FIXME: Call functions from SWATCH-independent board control libraries
  //        that power off data-processing FPGA (and supporting components)
  //        Access parameters as follows: params.get<TYPE>("myParameter")
  //        If this command takes longer than a few seconds, declare progress using setProgress member function

  std::cout << "Hello world" << std::endl;
  setStatusMsg("Hello world 2");

  std::system("touch dat/crap");
  
  // Declare that main FW components are non-functional after powering off
  using namespace ::swatch::phase2::processorIds;
  disableMonitoring({ kReadout, kInputPorts, kOutputPorts, kTTC });

  // FIXME: Return State::kError if error occurred, State::kDone if successful, or
  //        State::kWarning if FPGA powered off but something abnormal and worrisome happened
  return State::kDone;
}


} // namespace commands
} // namespace swatch
} // namespace exampleboard
