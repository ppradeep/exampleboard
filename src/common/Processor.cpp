#include "exampleboard/swatch/Processor.hpp"

#include "swatch/core/Factory.hpp"
#include "swatch/phase2/InputPortCollection.hpp"
#include "swatch/phase2/OutputPortCollection.hpp"

#include "exampleboard/swatch/InputPort.hpp"
#include "exampleboard/swatch/OutputPort.hpp"
#include "exampleboard/swatch/Readout.hpp"
#include "exampleboard/swatch/TCDS.hpp"
#include "exampleboard/swatch/commands/PowerOff.hpp"
#include "exampleboard/swatch/commands/PowerOn.hpp"
#include "exampleboard/swatch/commands/Program.hpp"

#include "exampleboard/swatch/commands/FsmBase.h"
//#include "exampleboard/swatch/commands/Wrapper.hpp"


SWATCH_REGISTER_CLASS(exampleboard::swatch::Processor)

namespace exampleboard {
namespace swatch {

using namespace ::swatch;

Processor::Processor(const ::swatch::core::AbstractStub& aStub) :
  phase2::Processor(aStub)
{
  // FIXME: If the data-processing FPGA is mounted on an optional module (e.g. mezzanine)
  //        then throw an instance of the ::swatch::phase2::ProcessorAbsent exception from
  //        this constructor if the module is not currently present.

  // 1) Declare commands
  registerCommand<commands::PowerOff>("powerOff");
  registerCommand<commands::PowerOn>("powerOn");
  registerCommand<commands::Program>("program");
  
  registerCommand<testsubsystem::swatch::commands::CommandDataTakingFsmInitialize>(testsubsystem::swatch::commands::CommandDataTakingFsmInitialize::commandId());
  registerCommand<testsubsystem::swatch::commands::CommandDataTakingFsmConfigure>( testsubsystem::swatch::commands::CommandDataTakingFsmConfigure::commandId());
  registerCommand<testsubsystem::swatch::commands::CommandDataTakingFsmStart>(     testsubsystem::swatch::commands::CommandDataTakingFsmStart::commandId());
  registerCommand<testsubsystem::swatch::commands::CommandDataTakingFsmPause>(     testsubsystem::swatch::commands::CommandDataTakingFsmPause::commandId());
  registerCommand<testsubsystem::swatch::commands::CommandDataTakingFsmResume>(    testsubsystem::swatch::commands::CommandDataTakingFsmResume::commandId());
  registerCommand<testsubsystem::swatch::commands::CommandDataTakingFsmStop>(      testsubsystem::swatch::commands::CommandDataTakingFsmStop::commandId());
  registerCommand<testsubsystem::swatch::commands::CommandDataTakingFsmHalt>(      testsubsystem::swatch::commands::CommandDataTakingFsmHalt::commandId());
  registerCommand<testsubsystem::swatch::commands::CommandDataTakingFsmReset>(     testsubsystem::swatch::commands::CommandDataTakingFsmReset::commandId());
  registerCommand<testsubsystem::swatch::commands::CommandDataTakingFsmEnd>(       testsubsystem::swatch::commands::CommandDataTakingFsmEnd::commandId());


  swatch::action::StateMachine& testFSM = registerStateMachine("DataTakingFsm", "Initial", "Error");

  testFSM.addState("Initial");
  testFSM.addState("Halted");


  testFSM.addTransition("Initialize", "Initial", "Halted").add(getCommand(testsubsystem::swatch::commands::CommandDataTakingFsmInitlaize::commandId()));
  testFSM.addTransition("Reset", "Halted", "Initial").add(getCommand(testsubsystem::swatch::commands::CommandDataTakingFsmReset::commandId()));

  
  // FIXME: Add commands for other parts of configuration sequence
  //        (E.g. configuring TCDS interface, configuring FPGA MGT RX/TX, configuring optics)

  // 2) Declare FSMs
  // FIXME: After relevant commands have been implemented, add these commands to setup and
  //        playback transitions of pre-defined single-board test FSM

  // 3) Declare classes representing main FW components
  // NOTE: You may want to change the signature of the constructors for these classes to
  //       to pass to them some lower level SWATCH-independent classes that represent this
  //       aspect of the firmware or that handle communication with the FW in general
  registerReadout(new Readout());
  registerTTC(new TCDS());
  auto& inputs = registerInputs();
  auto& outputs = registerOutputs();

  // This for loop creates 36 instances of the InputPort classes referred to as FPGA channels 0 to 35,
  // and declares the following simple optics-FPGA channel mapping:
  //    * Firefly A, channel 0  -> FPGA channel 0
  //    * Firefly A, channel 1  -> FPGA channel 1
  //       ...
  //    * Firefly A, channel 11 -> FPGA channel 11
  //    * Firefly B, channel  0 -> FPGA channel 12
  //       ...
  //    * Firefly B, channel 11 -> FPGA channel 23
  //    * Firefly C, channel  0 -> FPGA channel 24
  //       ...
  //    * Firefly C, channel 11 -> FPGA channel 35
  // For a real board, you'll need to at least change number of loop iterations to the max number
  // of supported MGT RX channels and change the optics <-> MGT channel mappings to match your board
  for (size_t i = 0; i < 36; i++) {
    const std::string opticsModuleName(i < 12 ? "A" : (i < 24 ? "B" : "C"));
    inputs.addPort(new InputPort(i, { "A", i % 12 }));
  }

  // This for loop creates 12 instances of the Output classes referred to as FPGA channels 0 to 11,
  // and declares the following simple optics-FPGA channel mapping
  //    * Firefly D, channel 0  -> FPGA channel 11
  //    * Firefly D, channel 1  -> FPGA channel 10
  //       ...
  //    * Firefly D, channel 11 -> FPGA channel 0
  // For a real board, you'll need to at least change number of loop iterations to the max number
  // of supported MGT TX channels and change the optics <-> MGT channel mappings to match your board
  for (size_t i = 0; i < 12; i++)
    outputs.addPort(new OutputPort(i, { "D", 11 - i }));
}


Processor::~Processor()
{
}


void Processor::retrievePropertyValues()
{
}


void Processor::retrieveMetricValues()
{
  // FIXME: Read monitoring data from FPGA, and set values of corresponding metrics - if any declared in this class
}

} // namespace swatch
} // namespace exampleboard
