
#include "swatch/phase2/Board.hpp"
#include "swatch/phase2/EmptySocket.hpp"

#include "exampleboard/swatch/FireflyRx.hpp"
#include "exampleboard/swatch/FireflyTx.hpp"


namespace exampleboard {
namespace swatch {

std::pair<std::vector<::swatch::phase2::OpticalModule*>, std::vector<std::string>> createOpticalModules()
{
  // FIXME: You will need to update this function to create a list of OpticalModule-derived objects
  //        that represent the optical modules on your board.
  //
  //        For illustrative purposes, the example code below describes a board with 6 sites for
  //        12-channel unidirectional Firefly modules (named A to F) and 6 front panel MTP connectors
  //        labelled "0" to "5". A RX module is plugged in at sites A, B and C, a TX module is plugged
  //        in at site D, and the other sites are empty (but designed to host a TX module). The optical
  //        module <-> front panel connection map is stored in etc/frontpanel.txt
  //
  //        Notes for re-writing this function board: The FireflyRx and FireflyTx classes are minimal
  //        placeholders written to allow the code below to compile. They may need to be renamed to
  //        match the optical modules used on your board. They will need to be updated to declare all
  //        relevant monitoring data to the framework - and read that data from the hardware. Their
  //        IDs strings should be names of sites that are meaningful to typical test stand operators/
  //        users - e.g. those printed on the PCB silkscreen. Finally, if a particular site is empty,
  //        please still add it to the list, but use the swatch::phase2::EmptySocket class (as is done
  //        below for sites E and F).

  std::vector<::swatch::phase2::OpticalModule*> modules;

  modules.push_back(new FireflyRx("A"));
  modules.push_back(new FireflyRx("B"));
  modules.push_back(new FireflyRx("C"));
  modules.push_back(new FireflyTx("D"));
  modules.push_back(new ::swatch::phase2::EmptySocket("E", 0 /* N_inputs */, 12 /* N_outputs */));
  modules.push_back(new ::swatch::phase2::EmptySocket("F", 0 /* N_inputs */, 12 /* N_outputs */));

  return { modules, { "0", "1", "2", "3", "4", "5" } };
}

} // namespace swatch
} // namespace exampleboard

SWATCH_REGISTER_PHASE2_OPTICS_CREATOR("ExampleBoard", exampleboard::swatch::createOpticalModules);
