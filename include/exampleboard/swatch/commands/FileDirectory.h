#ifndef Hgcal10gLinkReceiver_FileDirectory_h
#define Hgcal10gLinkReceiver_FileDirectory_h

#include <iostream>
#include <fstream>
#include <sstream>

#include "../NamespaceStart.hpp"
namespace commands {

  class FileDirectory {
  public:
    
    FileDirectory() {
    }
    
    static std::string getRelayDirectory(uint32_t r) {
      std::ostringstream sRelayDirectory;
      sRelayDirectory << std::setfill('0')
		      << "dat/Relay"
		      << std::setw(10) << r << "/";
      return sRelayDirectory.str();
    }

    static std::string getRelayFileName(uint32_t r) {
      std::ostringstream sRelayFileName;
      sRelayFileName << std::setfill('0')
		     << getRelayDirectory(r)
		     << "Relay" << std::setw(10) << r
		     << ".bin";
      return sRelayFileName.str();
    }
    
    static std::string getRunFileName(uint32_t r, uint32_t l, uint32_t f) {
      std::ostringstream sRunFileName;
      sRunFileName << std::setfill('0')
		   << getRelayDirectory(r)
		   << "Run" << std::setw(10) << r
		   << "_Link" << std::setw(2) << l
		   << "_File" << std::setw(10) << f
		   << ".bin";
      return sRunFileName.str();
    }
  };

}
#include "../NamespaceEnd.hpp"

#endif
