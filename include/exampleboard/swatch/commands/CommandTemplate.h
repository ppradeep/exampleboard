#ifndef CommandTemplate_h
#define CommandTemplate_h

#include <iostream>
#include <string>

#include "swatch/action/Command.hpp"
#include "swatch/action/Functionoid.hpp"

#include "exampleboard/swatch/NamespaceStart.hpp"

//namespace testsubsystem {
//namespace swatch {
namespace commands {

  //template<class T, typename T::Command C> class CommandTemplate : public T {
template<class T, class F, typename F::FSMCommand C> class CommandTemplate : public T {
public:
  CommandTemplate(const std::string& aId, ::swatch::action::ActionableObject& aActionable): T(aId, commandId(), aActionable)
  {
    T::defineParameters(C);
  }

  virtual ~CommandTemplate(){
  }

  static std::string commandId() {
    return T::baseName()+F::commandName(C);
  }
  
  virtual ::swatch::action::Functionoid::State code(const ::swatch::core::ParameterSet& aParams) {
    std::ostringstream sout;
    sout << "Calling obey() for command " << F::commandName(C);
    this->setStatusMsg(sout.str());
    
    return T::obey(C, aParams);
    //return ::swatch::action::Functionoid::State::kDone;
  }
  /*
  void passstring(const std::string& in){
    this->setStatusMsg(in);
  }
  */
};

}
//}
//}
#include "exampleboard/swatch/NamespaceEnd.hpp"

#endif
