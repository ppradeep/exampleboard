#ifndef FsmEnum_h
#define FsmEnum_h

enum Command {
  Initialize,
  Configure,
  Start,
  Pause,
  Resume,
  Stop,
  Halt,
  Reset,

  EndOfCommandEnum
};

enum State {

  // Static
  Initial,
  Halted,
  Configured,
  Running,
  Paused,

  // Transient
  Initializing,
  Configuring,
  Reconfiguring,
  Starting,
  Pausing,
  Resuming,
  Stopping,
  Halting,
  Resetting,

  EndOfStateEnum
};

#endif
