#ifndef CommandDataTakingFsmDaqPcBase_h
#define CommandDataTakingFsmDaqPcBase_h

#include <bits/stdc++.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <fcntl.h>
#include <sys/time.h>

#include <iostream>
#include <fstream>
#include <cassert>
#include <thread>

#include "yaml-cpp/yaml.h"

#include "swatch/action/Command.hpp"
#include "DataFifo.h"
#include "FsmState.h"
#include "CommandDummy.hpp"

#include "CommandTemplate.h"

//#include "/hgcal10glinkreceiver/common/inc/RecordYaml.h"

//#include "ProcessorRelayWriter.h"


#include "exampleboard/swatch/NamespaceStart.hpp"

//namespace testsubsystem {
// namespace swatch {
   namespace commands {

      typedef Hgcal10gLinkReceiver::DataFifoT<4,8*1024> DataFifo;
      typedef std::array<uint64_t,128*1024> DataBuffer;
     
     template <class T> class TEMPNAME : public ::swatch::action::Command {
      public:
	enum FSMCommand {
	  Initialize,
	  Configure,
	  Start,
	  Pause,
	  Resume,
	  Stop,
	  Halt,
	  Reset,
	  End,

	  EndOfCommandEnum
	};

	/*
	  enum State {

	  // Static
	  Initial,
	  Halted,
	  Configured,
	  Running,
	  Paused,

	  // Transient
	  Initializing,
	  Configuring,
	  Reconfiguring,
	  Starting,
	  Pausing,
	  Resuming,
	  Stopping,
	  Halting,
	  Resetting,

	  EndOfStateEnum
	  };
	*/


	class ThreadFsm {
	public:
	  std::thread _thread;
	  FsmState::State _state;
	  FsmState::FSMCommand _command;
	};
       /*
	CommandDataTakingFsmDaqPcBase(const std::string& aId, const std::string& aDescription, ::swatch::action::ActionableObject& aActionable) : Command(aId, aDescription, aActionable, std::string())
	{
	}

	virtual ~CommandDataTakingFsmDaqPcBase() {
	}
       */
	TEMPNAME(const std::string& aId, const std::string& aDescription, ::swatch::action::ActionableObject& aActionable) : Command(aId, aDescription, aActionable, std::string())
	{
	}

	virtual ~TEMPNAME() {
	}

	static std::string baseName() {
	  return std::string("Command")+FsmState::fsmName()+"DaqPc";
	}

	void defineParameters(FsmState::FSMCommand c) {
	  setStatusMsg("Entering defineParameters");

	  switch(c) {
      
	  case FsmState::Initialize: {
	    setStatusMsg("Entering Initialize");
	    break;
	  }
      
	  case FsmState::Configure: {
	    setStatusMsg("Entering Configure");
	    registerParameter<uint32_t>("Relay number",0xffffffff);
	    registerParameter<bool>("Write to disk? 0=false,1=true",true);
	    registerParameter<std::string>("Relay type","Pedestals");
	    //registerParameter<uint32_t>("Run time (sec)",0xffffffff);
	    break;
	  }
      
	  case FsmState::Start: {
	    setStatusMsg("Entering Start");
	    registerParameter<uint32_t>("Run number",0xffffffff);
	    break;
	  }
    
	  case FsmState::Pause: {
	    setStatusMsg("Entering Pause");
	    break;
	  }
      
	  case FsmState::Resume: {
	    setStatusMsg("Entering Resume");
	    break;
	  }
    
	  case FsmState::Stop: {
	    setStatusMsg("Entering Stop");
	    break;
	  }
    
	  case FsmState::Halt: {
	    setStatusMsg("Entering Halt");
	    break;
	  }

	  case FsmState::Reset: {
	    setStatusMsg("Entering Reset");
	    break;
	  }

	  case FsmState::End: {
	    setStatusMsg("Entering End");
	    registerParameter<bool>("Are you sure you want to shutdown?",true);
	    break;
	  }

	  default: {
	    break;
	  }
	  }
	}
  
	::swatch::action::Functionoid::State obey(FsmState::FSMCommand c, const ::swatch::core::ParameterSet& params) {
	  {
	  std::ostringstream sout;
	  sout << "Starting obey(): command " << FsmState::commandName(c) 
	       << ", current state " << FsmState::stateName(overallState);
	      
	  setStatusMsg(sout.str());
	  }
	  
	  if(!FsmState::allowedChange(overallState,c)) {
	    std::ostringstream sout;
	    sout << "Command not allowed, remain in current state "
		 << FsmState::stateName(overallState);
	    setStatusMsg(sout.str());
	    return ::swatch::action::Functionoid::State::kWarning;
	  }
	  
	  switch(c) {

	  case FsmState::Initialize: {
	    setStatusMsg("Entering Initialize");
	    _templateCommand.initialize();
	    overallState=FsmState::Halted;
	    break;
	  }
      
	  case FsmState::Configure: {
	    setStatusMsg("Entering Configure");

	    overallState=FsmState::Configured;
	    break;
	  }
      
	  case FsmState::Start: {
	    setStatusMsg("Entering Start");

	    overallState=FsmState::Running;
	    break;
	  }
    
	  case FsmState::Pause: {
	    setStatusMsg("Entering Pause");

	    overallState=FsmState::Paused;
	    break;
	  }
      
	  case FsmState::Resume: {
	    setStatusMsg("Entering Resume");

	    overallState=FsmState::Running;
	    break;
	  }
    
	  case FsmState::Stop: {
	    setStatusMsg("Entering Stop");

	    overallState=FsmState::Configured;
	    break;
	  }
      
	  case FsmState::Halt: {
	    setStatusMsg("Entering Halt");

	    overallState=FsmState::Halted;
	    break;
	  }
    
	  case FsmState::Reset: {
	    setStatusMsg("Entering Reset");

	    overallState=FsmState::Initial;
	    break;
	  }
    
	  case FsmState::End: {
	    setStatusMsg("Entering End");

	    if(params.get<bool>("Are you sure you want to shutdown?")) {
	      exit(0);
	    }

	    overallState=FsmState::Shutdown;
	    break;
	  }

	  default: {
	    setStatusMsg("Entering Default");

	    return ::swatch::action::Functionoid::State::kError;
	    break;
	  }
	  }

	  {
	  std::ostringstream sout;
	  sout << "Leaving obey(): new state "
	       << FsmState::stateName(overallState);
	  setStatusMsg(sout.str());
	  }
	  return ::swatch::action::Functionoid::State::kDone;
	}

	static std::string commandName(FSMCommand c) {
	  return (c<EndOfCommandEnum?_commandName[c]:"Unknown");
	}
  
      protected:
	static FsmState _state;
       static T _templateCommand;
	static FsmState::State overallState;

	static std::string _commandName[EndOfCommandEnum];
      };

     typedef TEMPNAME<CommandDummy> CommandDataTakingFsmDaqPcBase;

     template<class T> T TEMPNAME<T>::_templateCommand;


     template<class T>
     FsmState::State TEMPNAME<T>::overallState(FsmState::Initial);

     template<class T>
      std::string TEMPNAME<T>::_commandName[EndOfCommandEnum]={
	"Initialize",
	"Configure",
	"Start",
	"Pause",
	"Resume",
	"Stop",
	"Halt",
	"Reset",
	"End"
      };

     typedef CommandTemplate<CommandDataTakingFsmDaqPcBase,FsmState,FsmState::Initialize> CommandDataTakingFsmInitialize;
     typedef CommandTemplate<CommandDataTakingFsmDaqPcBase,FsmState,FsmState::Configure>  CommandDataTakingFsmConfigure;
     typedef CommandTemplate<CommandDataTakingFsmDaqPcBase,FsmState,FsmState::Start>      CommandDataTakingFsmStart;
     typedef CommandTemplate<CommandDataTakingFsmDaqPcBase,FsmState,FsmState::Pause>      CommandDataTakingFsmPause;
     typedef CommandTemplate<CommandDataTakingFsmDaqPcBase,FsmState,FsmState::Resume>     CommandDataTakingFsmResume;
     typedef CommandTemplate<CommandDataTakingFsmDaqPcBase,FsmState,FsmState::Stop>       CommandDataTakingFsmStop;
     typedef CommandTemplate<CommandDataTakingFsmDaqPcBase,FsmState,FsmState::Halt>       CommandDataTakingFsmHalt;
     typedef CommandTemplate<CommandDataTakingFsmDaqPcBase,FsmState,FsmState::Reset>      CommandDataTakingFsmReset;
     typedef CommandTemplate<CommandDataTakingFsmDaqPcBase,FsmState,FsmState::End>        CommandDataTakingFsmEnd;

   }

//  }
//}
#include "exampleboard/swatch/NamespaceEnd.hpp"

#endif
