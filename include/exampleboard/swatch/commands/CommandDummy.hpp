#ifndef Hgcal10gLinkReceiver_CommandDummy_h
#define Hgcal10gLinkReceiver_CommandDummy_h

#include <iostream>
#include <iomanip>
#include <cstdint>
#include <cstring>

#include "../NamespaceStart.hpp"
namespace commands {

  class CommandDummy {

  public:
    CommandDummy() {
    }

    virtual ~CommandDummy() {
    }

    bool initialize() {
      std::cout << "HERE" << std::endl;
      return true;
    }
    
    bool configure() {
      return true;
    }

    bool reconfigure() {
      return true;
    }

    bool start() {
      return true;
    }
    
    bool pause() {
      return true;
    }
    
    bool resume() {
      return true;
    }
    
    bool stop() {
      return true;
    }    

    bool halt() {
      return true;
    }    

    bool reset() {
      return true;
    }
   
    bool end() {
      return true;
    }    

  private:
  };

}
#include "../NamespaceEnd.hpp"

#endif
