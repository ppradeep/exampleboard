#ifndef __EXAMPLEBOARD_SWATCH_COMMANDS_POWEROFF_HPP__
#define __EXAMPLEBOARD_SWATCH_COMMANDS_POWEROFF_HPP__

#include "swatch/action/Command.hpp"


namespace exampleboard {
namespace swatch {
namespace commands {

class PowerOff : public ::swatch::action::Command {
public:
  PowerOff(const std::string&, ::swatch::action::ActionableObject&);

  virtual ~PowerOff();

private:
  virtual State code(const ::swatch::core::ParameterSet&);
};

} // namespace commands
} // namespace swatch
} // namespace exampleboard


#endif