#ifndef FsmBase_h
#define FsmBase_h

#include <bits/stdc++.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>

#include <iostream>
#include <fstream>
#include <cassert>
#include <thread>

#include "yaml-cpp/yaml.h"

#include "swatch/action/Command.hpp"
#include "DataFifo.h"
#include "FsmState.h"

//#include "/hgcal10glinkreceiver/common/inc/RecordYaml.h"

//#include "ProcessorRelayWriter.h"

#include "exampleboard/swatch/NamespaceStart.hpp"

//namespace testsubsystem {
// namespace swatch {
   namespace commands {

      typedef Hgcal10gLinkReceiver::DataFifoT<4,8*1024> DataFifo;
      typedef std::array<uint64_t,128*1024> DataBuffer;

      class FsmBase : public ::swatch::action::Command {
      public:
	enum FSMCommand {
	  Initialize,
	  Configure,
	  Start,
	  Pause,
	  Resume,
	  Stop,
	  Halt,
	  Reset,
	  End,

	  EndOfCommandEnum
	};

	/*
	  enum State {

	  // Static
	  Initial,
	  Halted,
	  Configured,
	  Running,
	  Paused,

	  // Transient
	  Initializing,
	  Configuring,
	  Reconfiguring,
	  Starting,
	  Pausing,
	  Resuming,
	  Stopping,
	  Halting,
	  Resetting,

	  EndOfStateEnum
	  };
	*/


	class ThreadFsm {
	public:
	  std::thread _thread;
	  FsmState::State _state;
	  FsmState::FSMCommand _command;
	};

	FsmBase(const std::string& aId, const std::string& aDescription, ::swatch::action::ActionableObject& aActionable) : Command(aId, aDescription, aActionable, std::string())
	{
	}

	virtual ~FsmBase() {
	}
  
	static std::string fsmName() {
	  return std::string("DataTakingFsm");
	}

	static std::string baseName() {
	  return FsmState::fsmName()+"DaqPc";
	}

	void defineParameters(FsmState::FSMCommand c) {
	  setStatusMsg("Entering defineParameters");

	  switch(c) {
      
	  case FsmState::Initialize: {
	    setStatusMsg("Entering Initialize");
	    break;
	  }
      
	  case FsmState::Configure: {
	    setStatusMsg("Entering Configure");
	    //registerParameter<uint32_t>("Relay number",0xffffffff);
	    registerParameter<bool>("Write to disk? 0=false,1=true",true);
	    registerParameter<std::string>("Relay type","Pedestals");
	    //registerParameter<uint32_t>("Run time (sec)",0xffffffff);
	    break;
	  }
      
	  case FsmState::Start: {
	    setStatusMsg("Entering Start");
	    //registerParameter<uint32_t>("Run number",0xffffffff);
	    break;
	  }
    
	  case FsmState::Pause: {
	    setStatusMsg("Entering Pause");
	    break;
	  }
      
	  case FsmState::Resume: {
	    setStatusMsg("Entering Resume");
	    break;
	  }
    
	  case FsmState::Stop: {
	    setStatusMsg("Entering Stop");
	    break;
	  }
    
	  case FsmState::Halt: {
	    setStatusMsg("Entering Halt");
	    break;
	  }

	  case FsmState::Reset: {
	    setStatusMsg("Entering Reset");
	    break;
	  }

	  case FsmState::End: {
	    setStatusMsg("Entering End");
	    registerParameter<bool>("Are you sure you want to shutdown?",true);
	    break;
	  }

	  default: {
	    break;
	  }
	  }
	}
  
	::swatch::action::Functionoid::State obey(FsmState::FSMCommand c, const ::swatch::core::ParameterSet& params) {
	  setStatusMsg("Entering obey");

	  switch(c) {

	  case FsmState::Initialize: {
	    setStatusMsg("Entering Initialize");

	    // Read in required initialization information
	    YAML::Node yn(YAML::LoadFile("DaqPcInitialization.yaml"));
	    std::ostringstream sout;
	    sout << yn;
	    std::cout << sout.str() << std::endl;
	    assert(yn["Source"].as<std::string>()=="DaqPc");

	    // Set up arrays for threads
	    //unsigned nSlinks(4);
	    unsigned nSlinks(yn["NumberOfSlinks"].as<unsigned>());
	    dthReceiverThreadFsmBuffer.resize(nSlinks);
	    dthReceiverThreadFsm.resize(nSlinks);
	    dthReceiverThreadFsmPort.resize(nSlinks);
	    dthReceiverThreadFsmConnect.resize(nSlinks);
	    runWriterThreadFsm.resize(nSlinks);
	    dataFifo.resize(nSlinks);
      
	    for(unsigned s(0);s<dthReceiverThreadFsm.size();s++) {
	      assert(!dthReceiverThreadFsm[s]._thread.joinable());	       

	      std::ostringstream sout;
	      sout << "Slink" << s;
	      dthReceiverThreadFsmPort[s]=yn[sout.str()]["Port"].as<uint16_t>();
	      
	      dthReceiverThreadFsm[s]._state=FsmState::Initial;
	      dthReceiverThreadFsm[s]._command=FsmState::EndOfCommandEnum;

	      dataFifo[s].coldStart();
	      dataFifo[s].print();
	    }
      
	    for(unsigned s(0);s<runWriterThreadFsm.size();s++) {
	      assert(!runWriterThreadFsm[s]._thread.joinable());
	    }

	    // Set relay constants
	    //_processorRelayWriter.setStuff();

	    // Start DTH receiver threads
	    for(unsigned s(0);s<dthReceiverThreadFsm.size();s++) {
	      dthReceiverThreadFsm[s]._command=FsmState::Initialize;
	      dthReceiverThreadFsm[s]._thread=std::thread(dthReceiverThreadMethod,s);
	      //Broken (Set back to static function to work and remove "*this")
	      //dthReceiverThreadFsm[s]._thread=std::thread(FsmBase::dthReceiverThreadMethod, *this, s);
	    }
	    
	    // Wait for DTH threads to complete initialization
	    for(unsigned s(0);s<dthReceiverThreadFsm.size();s++) {
	      while(dthReceiverThreadFsm[s]._state!=FsmState::Halted) sleep(1);
	      dthReceiverThreadFsm[s]._command=FsmState::EndOfCommandEnum;
	    }

	    // Set up configuration counter
	    _configurationNumberInRelay=0xffffffff;

	    // Call relay writer
	    //_processorRelayWriter.initialize();

	    break;
	  }
      
	  case FsmState::Configure: {
	    setStatusMsg("Entering Configure");

	    for(unsigned s(0);s<dthReceiverThreadFsm.size();s++) dthReceiverThreadFsm[s]._command=c;
	    
	    _runNumberInRelay=0;

	    // First configuration
	    if(_configurationNumberInRelay==0xffffffff) {

	      if(params.get<bool>("Write to disk? 0=false,1=true")) _relayNumber=time(0);
	      else _relayNumber=0xffffffff;

	      // Need to connect these up somehow
	      //params.get<uint32_t>("Run time (sec)");

	      YAML::Node yn;
	      yn["RelayNumber"]=_relayNumber;
	      yn["RelayType"]=params.get<std::string>("Relay type");

	      std::cout << yn << std::endl;

	      std::ostringstream sout;
	      sout << yn;

	      //_recordYaml.reset(FsmState::Configuring);
	      //_recordYaml.setUtc();
	      //_recordYaml.setString(sout.str());
	      //_recordYaml.print();

	
	      if(_relayNumber<0xffffffff) {
		std::ostringstream sout;
		sout << "dat/Relay" << std::setfill('0')
		     << std::setw(10) << _relayNumber;
	  
		_relayDirectory=sout.str();
		std::system((std::string("mkdir ")+_relayDirectory).c_str());
	  
		sout << "/Relay" << std::setfill('0')
		     << std::setw(10) << _relayNumber << ".bin";
	  
		std::system((std::string("touch ")+sout.str()).c_str());
	      }
	      /*

		_fileWriter.setDirectory(sout.str());
	    
		std::ofstream fout((sout.str()+"/Definition.yaml").c_str());
		fout << nRsa << std::endl;
		fout.close();
		}
	    
		_fileWriter.openRelay(_relayNumber);
		_fileWriter.write(&(_ptrFsmInterface->record()));
	      */
	      /*
		relayWriterState=Configuring;
		relayWriterThread=std::thread(relayWriterThreadMethod,nRelay);
		while(relayWriterState!=Configured) sleep(1);
	      */
	      _configurationNumberInRelay=0;

	    } else {
      
	      // Increment configuration counter
	      _configurationNumberInRelay++;
	    }
	
	    // Wait for DTH threads to complete configuration
	    for(unsigned s(0);s<dthReceiverThreadFsm.size();s++) {
	      while(dthReceiverThreadFsm[s]._state!=FsmState::Configured) sleep(1);
	      dthReceiverThreadFsm[s]._command=FsmState::EndOfCommandEnum;
	    }

	    // Call relay writer
	    //_processorRelayWriter.configure();

	    break;
	  }
      
	  case FsmState::Start: {
	    setStatusMsg("Entering Start");

	    for(unsigned s(0);s<dthReceiverThreadFsm.size();s++) dthReceiverThreadFsm[s]._command=c;

	    _runNumber=time(0);
	    //_runNumber=params.get<uint32_t>("Run number");

	    YAML::Node yn;
	    yn["RunNumber"]=_runNumber;
	    yn["RunNumberInRelay"]=_runNumberInRelay;

	    std::cout << yn << std::endl;
      
	    _runNumberInRelay++;

	    // Start run writer threads
	    for(unsigned s(0);s<runWriterThreadFsm.size();s++) {
	      runWriterThreadFsm[s]._state=FsmState::Configured;
	      runWriterThreadFsm[s]._command=FsmState::Start;
	      runWriterThreadFsm[s]._thread=std::thread(runWriterThreadMethod,s);
	    }

	    for(unsigned s(0);s<runWriterThreadFsm.size();s++) {
	      while(runWriterThreadFsm[s]._state!=FsmState::Running) sleep(1);
	      runWriterThreadFsm[s]._command=FsmState::EndOfCommandEnum;
	    }
     
	    /*
	      for(unsigned s(0);s<runWriterState.size();s++) {
	      runWriterState[s]=Starting;
	      runWriterThread[s]=
	      }
    
	      for(unsigned s(0);s<runWriterState.size();s++) {
	      while(runWriterState[s]!=Running) sleep(1);
	      }
	    */

	    // Wait for DTH threads to complete start
	    for(unsigned s(0);s<dthReceiverThreadFsm.size();s++) {
	      while(dthReceiverThreadFsm[s]._state!=FsmState::Running) sleep(1);
	      dthReceiverThreadFsm[s]._command=FsmState::EndOfCommandEnum;
	    }

	    // Call relay writer
	    //_processorRelayWriter.start();

	    break;
	  }
    
	  case FsmState::Pause: {

	    // Call relay writer
	    //_processorRelayWriter.pause();

	    break;
	  }
      
	  case FsmState::Resume: {

	    // Call relay writer
	    //_processorRelayWriter.resume();

	    break;
	  }
    
	  case FsmState::Stop: {
	    setStatusMsg("Entering Stop");
	    for(unsigned s(0);s<dthReceiverThreadFsm.size();s++) dthReceiverThreadFsm[s]._command=c;

	    // Start run writer threads
	    for(unsigned s(0);s<runWriterThreadFsm.size();s++) {
	      runWriterThreadFsm[s]._command=FsmState::Stop;
	    }

	    for(unsigned s(0);s<runWriterThreadFsm.size();s++) {
	      runWriterThreadFsm[s]._thread.join();
	      runWriterThreadFsm[s]._command=FsmState::EndOfCommandEnum;
	    }
	    /*
	      for(unsigned s(0);s<runWriterState.size();s++) {
	      runWriterState[s]=Stopping;
	      }

	      for(unsigned s(0);s<runWriterState.size();s++) {
	      runWriterThread[s].join();
	      }
	    */

	    // Wait for DTH threads to complete stop
	    for(unsigned s(0);s<dthReceiverThreadFsm.size();s++) {
	      while(dthReceiverThreadFsm[s]._state!=FsmState::Configured) sleep(1);
	      dthReceiverThreadFsm[s]._command=FsmState::EndOfCommandEnum;
	    }

	    // Call relay writer
	    //_processorRelayWriter.stop();

	    break;
	  }
      
	  case FsmState::Halt: {
	    setStatusMsg("Entering Halt");
	    for(unsigned s(0);s<dthReceiverThreadFsm.size();s++) dthReceiverThreadFsm[s]._command=c;

	    //relayWriterState=Halting;
	    //relayWriterThread.join();
	    // Set up configuration counter
	    _configurationNumberInRelay=0xffffffff;

	    // Wait for DTH threads to complete halt
	    for(unsigned s(0);s<dthReceiverThreadFsm.size();s++) {
	      while(dthReceiverThreadFsm[s]._state!=FsmState::Halted) sleep(1);
	      dthReceiverThreadFsm[s]._command=FsmState::EndOfCommandEnum;
	    }

	    // Call relay writer
	    //_processorRelayWriter.halt();

	    break;
	  }
    
	  case FsmState::Reset: {
	    setStatusMsg("Entering Reset");
	    for(unsigned s(0);s<dthReceiverThreadFsm.size();s++) dthReceiverThreadFsm[s]._command=c;
	    //for(unsigned s(0);s<dthReceiverThreadFsm.size();s++) {
	    //  dthReceiverThreadFsm[s]._command=FsmState::Reset;
	    /// }

	    for(unsigned s(0);s<dthReceiverThreadFsm.size();s++) {
	      dthReceiverThreadFsm[s]._thread.join();
	      dthReceiverThreadFsm[s]._command=FsmState::EndOfCommandEnum;
	    }
	    /*
	      for(unsigned s(0);s<dthReceiverThreadFsm.size();s++) {
	      while(dthReceiverThreadFsm[s]._state!=FsmState::Halted) sleep(1);
	      }
	      for(unsigned s(0);s<dthReceiverState.size();s++) {
	      dthReceiverThread[s].join();
	      }
	    */
      
	    // Call relay writer
	    //_processorRelayWriter.reset();

	    break;
	  }
    
	  case FsmState::End: {
	    setStatusMsg("Entering End");
	    if(params.get<bool>("Are you sure you want to shutdown?")) exit(0);
	    break;
	  }

	  default: {
	    break;
	  }
	  }
	  
	  return ::swatch::action::Functionoid::State::kDone;
	}

	static void dthReceiverThreadMethod(unsigned slink);

	static void runWriterThreadMethod(unsigned slink);

	static std::string commandName(FSMCommand c) {
	  return (c<EndOfCommandEnum?_commandName[c]:"Unknown");
	}

	//static void print() {
	//  std::cout << anUnsigned << std::endl;
	// }
  
      protected:
	static FsmState _state;
  
	static unsigned para;
  
	static std::vector<DataBuffer> dthReceiverThreadFsmBuffer;
	static std::vector<uint16_t> dthReceiverThreadFsmPort;
	static std::vector<int> dthReceiverThreadFsmConnect;
	static std::vector<DataFifo> dataFifo;
	static std::vector<ThreadFsm> dthReceiverThreadFsm;
	static std::vector<ThreadFsm> runWriterThreadFsm;

	//static std::vector<std::thread> dthReceiverThread;
	//static std::thread relayWriterThread;
	//static std::vector<std::thread> runWriterThread;

	static uint32_t _relayNumber;
	static std::string _relayDirectory;
	static unsigned _configurationNumberInRelay;
	static unsigned _runNumberInRelay;
	static uint32_t _runNumber;

	FsmState::State overallState;
	static std::vector<FsmState::State> dthReceiverState;
	static FsmState::State relayWriterState;
	static FsmState::FSMCommand relayWriterCommand;
	//static std::vector<FsmState::State> runWriterState;

	//Hgcal10gLinkReceiver::RecordYaml _recordYaml;

	//static ProcessorRelayWriter _processorRelayWriter;

	static std::string _commandName[EndOfCommandEnum];
      };

      //Hgcal10gLinkReceiver::RecordYaml FsmBase::_recordYaml;

      unsigned FsmBase::para(0);

      unsigned FsmBase::_relayNumber;
      std::string FsmBase::_relayDirectory;
      unsigned FsmBase::_configurationNumberInRelay;
      unsigned FsmBase::_runNumberInRelay;
      unsigned FsmBase::_runNumber;

      //ProcessorRelayWriter FsmBase::_processorRelayWriter;

      //std::vector<std::thread> FsmBase::dthReceiverThread;
      //std::thread FsmBase::relayWriterThread;
      //std::vector<std::thread> FsmBase::runWriterThread;

      //std::vector<FsmBase::FsmState::State> FsmBase::dthReceiverState;
      //FsmBase::FsmState::State FsmBase::relayWriterState(FsmBase::Initial);
      //std::vector<FsmBase::FsmState::State> FsmBase::runWriterState;

      std::vector<DataBuffer> FsmBase::dthReceiverThreadFsmBuffer;
      std::vector<uint16_t> FsmBase::dthReceiverThreadFsmPort;
      std::vector<int> FsmBase::dthReceiverThreadFsmConnect;
      std::vector<DataFifo> FsmBase::dataFifo;
      std::vector<FsmBase::ThreadFsm> FsmBase::dthReceiverThreadFsm;
      std::vector<FsmBase::ThreadFsm> FsmBase::runWriterThreadFsm;

      std::string FsmBase::_commandName[EndOfCommandEnum]={
	"Initialize",
	"Configure",
	"Start",
	"Pause",
	"Resume",
	"Stop",
	"Halt",
	"Reset",
	"End"
      };

      ///////////////////////////////////////////////////////////////////////////

      /*
      void FsmBase::dthReceiverThreadMethodGetTcpPacket(const unsigned slink) {
	int n(0);

	while(n<8) { // Catch no packet
	  n = recv(connected,&(dthReceiverThreadFsmBuffer[slink]),sizeof(DataBuffer),0);
	  
	  if(n<8 || (n%8)!=0) std::cerr << "WARNING: number of bytes read = " << n << " gives n%8 = " << (n%8)
					<< " != 0" << std::endl;
	  if(n>=sizeof(DataBuffer)) std::cerr << "WARNING: number of bytes read = " << n << " >= "
				       << sizeof(DataBuffer) << std::endl;
	  
	  // Initialise number of words in TCP packet
	  n64Tcp=(n+7)/8;
	  i64Tcp=0;
	  
	  if(printEnable) {
	    std::cout  << std::endl << "************ GOT DATA ******************" << std::endl << std::endl;
	    
	    std::cout << "TCP size = " << n << " bytes = " << n64Tcp << " uint64_t words" << std::endl;
	    
	    // Short or long printout of buffer
	    if(false) {
	      if(n>= 8) std::cout << "First word        = " << std::hex << std::setw(16) << buffer[0] << std::dec << std::endl;
	      if(n>=16) std::cout << "Second word       = " << std::hex << std::setw(16) << buffer[1] << std::dec << std::endl;
	      if(n>=24) std::cout << "Third word        = " << std::hex << std::setw(16) << buffer[2] << std::dec << std::endl;
	      if(n>=32) std::cout << "Fourth word       = " << std::hex << std::setw(16) << buffer[3] << std::dec << std::endl;
	      if(n>=24) std::cout << "Last-but-one word = " << std::hex << std::setw(16) << buffer[n64Tcp-2] << std::dec << std::endl;
	      if(n>=32) std::cout << "Last word         = " << std::hex << std::setw(16) << buffer[n64Tcp-1] << std::dec << std::endl;
	      
	    } else {
	      for(unsigned i(0);i<n64Tcp;i++) {
		std::cout << "Word " << std::setw(6) << i << " = " << std::hex << std::setw(16) << buffer[i] << std::dec << std::endl;
	      }
	    }
	  }
	}
      }
      */      
      void FsmBase::dthReceiverThreadMethod(const unsigned slink) {
	std::ostringstream sout;

	sout << "FsmBase::dthReceiverThreadMethod: slink = " << slink
	     << ", state = " << FsmState::stateName(dthReceiverThreadFsm[slink]._state)
	     << ", command = " << FsmState::commandName(dthReceiverThreadFsm[slink]._command);

	//::swatch::action::Command::setStatusMsg(sout.str());
	std::cout << sout.str() << std::endl;
	sout.str("");
	sout.clear();

	assert(dthReceiverThreadFsm[slink]._state==FsmState::Initial);
	assert(dthReceiverThreadFsm[slink]._command==FsmState::Initialize);

	if(dthReceiverThreadFsmPort[slink]>0) {
	  int sock;
	  int istrue = 1;          
	  struct sockaddr_in server_addr, client_addr;    
	  unsigned int sin_size;
	  
	  
	  if ((sock = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
	    perror("socket");
	    exit(1);
	  }
	  
	  /* set SO_REUSEADDR on a socket to bypass TIME_WAIT state */
	  
	  if(false) {
	    if (setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &istrue, sizeof(int)) == -1) {
	      perror("setsockopt");
	      exit(1);
	    }
	  }
	  
	  server_addr.sin_family = AF_INET;         
	  server_addr.sin_port = htons(dthReceiverThreadFsmPort[slink]);     
	  server_addr.sin_addr.s_addr = INADDR_ANY; 
	  bzero(&(server_addr.sin_zero), 8); 
	  
	  if (bind(sock, (struct sockaddr *)&server_addr, sizeof(struct sockaddr)) == -1) {
	    perror("Unable to bind");
	    exit(1);
	  }
	  
	  if (listen(sock, 1) == -1) {
	    perror("Listen");
	    exit(1);
	  }
	  
	  printf("\nListening on port %u\n",dthReceiverThreadFsmPort[slink]);
	  sin_size = sizeof(struct sockaddr_in);
	  dthReceiverThreadFsmConnect[slink] = accept(sock, (struct sockaddr *)&client_addr,&sin_size);	  
	  printf("\nConnected...\n");
	  

	} else {

	  // Set all Record lengths in data FIFO to maximum
	  dataFifo[slink].setMaxPayloadLengths();
	}
	
	dthReceiverThreadFsm[slink]._command=FsmState::EndOfCommandEnum;
	dthReceiverThreadFsm[slink]._state=FsmState::Halted;

	sout << "FsmBase::dthReceiverThreadMethod: slink = " << slink
	     << ", state = " << FsmState::stateName(dthReceiverThreadFsm[slink]._state)
	     << ", command = " << FsmState::commandName(dthReceiverThreadFsm[slink]._command);

	//::swatch::action::Command::setStatusMsg(sout.str());
	std::cout << sout.str() << std::endl;
	sout.str("");
	sout.clear();
    
	while(dthReceiverThreadFsm[slink]._command!=FsmState::Reset) {
	  if(dthReceiverThreadFsm[slink]._command==FsmState::EndOfCommandEnum) {
	    sleep(1);
	    
	  } else {
	    assert(dthReceiverThreadFsm[slink]._state==FsmState::Halted);
	    assert(dthReceiverThreadFsm[slink]._command==FsmState::Configure);
	    
	    dthReceiverThreadFsm[slink]._command=FsmState::EndOfCommandEnum;
	    dthReceiverThreadFsm[slink]._state=FsmState::Configured;
	    
	    sout << "FsmBase::dthReceiverThreadMethod: slink = " << slink
		 << ", state = " << FsmState::stateName(dthReceiverThreadFsm[slink]._state)
		 << ", command = " << FsmState::commandName(dthReceiverThreadFsm[slink]._command);

	    //::swatch::action::Command::setStatusMsg(sout.str());
	    std::cout << sout.str() << std::endl;
	    sout.str("");
	    sout.clear();
    
	    while(dthReceiverThreadFsm[slink]._command!=FsmState::Halt) {
	      if(dthReceiverThreadFsm[slink]._command==FsmState::EndOfCommandEnum) {
		sleep(1);
		
	      } else {
		
		assert(dthReceiverThreadFsm[slink]._state==FsmState::Configured);
		assert(dthReceiverThreadFsm[slink]._command==FsmState::Start);
		
		dthReceiverThreadFsm[slink]._command=FsmState::EndOfCommandEnum;
		dthReceiverThreadFsm[slink]._state=FsmState::Running;
		
		sout << "FsmBase::dthReceiverThreadMethod: slink = " << slink
		     << ", state = " << FsmState::stateName(dthReceiverThreadFsm[slink]._state)
		     << ", command = " << FsmState::commandName(dthReceiverThreadFsm[slink]._command);
		
		//::swatch::action::Command::setStatusMsg(sout.str());
		std::cout << sout.str() << std::endl;
		sout.str("");
		sout.clear();
		
		while(dthReceiverThreadFsm[slink]._command!=FsmState::Stop) {
		  if(dthReceiverThreadFsmPort[slink]>0) {
		    sleep(1); // SHIFT DATA
		    
		  } else {
		    while(dataFifo[slink].writeable()) dataFifo[slink].writeIncrement();
		    dataFifo[slink].print();
		    //usleep(10000);
		    sleep(1);
		  }
		}

		assert(dthReceiverThreadFsm[slink]._state==FsmState::Running);
		assert(dthReceiverThreadFsm[slink]._command==FsmState::Stop);
		
		dthReceiverThreadFsm[slink]._command=FsmState::EndOfCommandEnum;
		dthReceiverThreadFsm[slink]._state=FsmState::Configured;
		
		sout << "FsmBase::dthReceiverThreadMethod: slink = " << slink
		     << ", state = " << FsmState::stateName(dthReceiverThreadFsm[slink]._state)
		     << ", command = " << FsmState::commandName(dthReceiverThreadFsm[slink]._command);
		
		//::swatch::action::Command::setStatusMsg(sout.str());
		std::cout << sout.str() << std::endl;
		sout.str("");
		sout.clear();
    	      }
	    }
	    assert(dthReceiverThreadFsm[slink]._state==FsmState::Configured);
	    assert(dthReceiverThreadFsm[slink]._command==FsmState::Helt);
	    
	    dthReceiverThreadFsm[slink]._command=FsmState::EndOfCommandEnum;
	    dthReceiverThreadFsm[slink]._state=FsmState::Halted;
	    
	    sout << "FsmBase::dthReceiverThreadMethod: slink = " << slink
		 << ", state = " << FsmState::stateName(dthReceiverThreadFsm[slink]._state)
		 << ", command = " << FsmState::commandName(dthReceiverThreadFsm[slink]._command);
		
	    //::swatch::action::Command::setStatusMsg(sout.str());
	    std::cout << sout.str() << std::endl;
	    sout.str("");
	    sout.clear();
	    
	  }
      
	  
	  //while(dthReceiverThreadFsm[slink]._command==FsmState::EndOfCommandEnum) {
	  //if(dthReceiverThreadFsm[slink]._command==FsmState::) {
	  /*
	    assert(dthReceiverState[slink]!=Initial);
	    assert(dthReceiverState[slink]!=Initializing);

	    if(dthReceiverState[slink]==Configuring  ) dthReceiverState[slink]=Configured;
	    if(dthReceiverState[slink]==Reconfiguring) dthReceiverState[slink]=Configured;
	    if(dthReceiverState[slink]==Starting     ) dthReceiverState[slink]=Running;
	    if(dthReceiverState[slink]==Pausing      ) dthReceiverState[slink]=Paused;
	    if(dthReceiverState[slink]==Resuming     ) dthReceiverState[slink]=Running;
	    if(dthReceiverState[slink]==Stopping     ) dthReceiverState[slink]=Configured;
	    if(dthReceiverState[slink]==Halting      ) dthReceiverState[slink]=Halted;
	  */    
	  //std::cout << "FsmBase::dthReceiverThreadMethod while: slink = " << slink << ", state = " << dthReceiverState[slink] << std::endl;
    
	}
      
	sout << "FsmBase::dthReceiverThreadMethod: slink = " << slink
	     << ", state = " << FsmState::stateName(dthReceiverThreadFsm[slink]._state)
	     << ", command = " << FsmState::commandName(dthReceiverThreadFsm[slink]._command);

	//::swatch::action::Command::setStatusMsg(sout.str());
	std::cout << sout.str() << std::endl;
	sout.str("");
	sout.clear();

	assert(dthReceiverThreadFsm[slink]._state==FsmState::Halted);
	assert(dthReceiverThreadFsm[slink]._command==FsmState::Reset);

	dthReceiverThreadFsm[slink]._command=FsmState::EndOfCommandEnum;
	dthReceiverThreadFsm[slink]._state=FsmState::Initial;

	sout << "FsmBase::dthReceiverThreadMethod: slink = " << slink
	     << ", state = " << FsmState::stateName(dthReceiverThreadFsm[slink]._state)
	     << ", command = " << FsmState::commandName(dthReceiverThreadFsm[slink]._command);

	//::swatch::action::Command::setStatusMsg(sout.str());
	std::cout << sout.str() << std::endl;
	sout.str("");
	sout.clear();
      }

      ///////////////////////////////////////////////////////////////////////////

      void FsmBase::runWriterThreadMethod(unsigned slink) {
	std::cout << "FsmBase::runWriterThreadMethod: slink = " << slink << std::endl;

	assert(runWriterThreadFsm[slink]._state==FsmState::Configured);
	assert(runWriterThreadFsm[slink]._command==FsmState::Start);

	std::cout << "FsmBase::runWriterThread start: slink = " << slink << std::endl;
	sleep(3); // Do start stuff

	if(_relayNumber<0xffffffff) {
	  std::ostringstream sout;
	  sout << _relayDirectory << "/Run"
	       << std::setfill('0') << std::setw(10) << _runNumber << "_Link"
	       << std::setfill('0') << std::setw(6) << slink << ".bin";
    
	  std::system((std::string("touch ")+sout.str()).c_str());
	}
  
	runWriterThreadFsm[slink]._command==FsmState::EndOfCommandEnum;
	runWriterThreadFsm[slink]._state=FsmState::Running;

	std::cout << "FsmBase::runWriterThread  done: slink = " << slink << std::endl;

	/*
	  #ifdef NOT_YET
	  _fileWriter.write(&(_ptrFsmInterface->record()));
	  #endif
	  return;
	  }
    
	  bool resume() {
	  #ifdef NOT_YET
	  _fileWriter.write(&(_ptrFsmInterface->record()));
	  #endif
	  return true;
	  }
	*/   
    
	while(runWriterThreadFsm[slink]._command==FsmState::EndOfCommandEnum) {

	  if(runWriterThreadFsm[slink]._command==FsmState::Reset) {
	    return;
	  }


	  if(runWriterThreadFsm[slink]._command==FsmState::Pause) {
	    assert(runWriterThreadFsm[slink]._state==FsmState::Running);

	    sleep(1); // Do pause stuff
    
	    runWriterThreadFsm[slink]._command=FsmState::EndOfCommandEnum;
	    assert(runWriterThreadFsm[slink]._state==FsmState::Paused);
    
	    while(runWriterThreadFsm[slink]._command==FsmState::EndOfCommandEnum) {
	      assert(runWriterThreadFsm[slink]._state==FsmState::Paused);
	      assert(runWriterThreadFsm[slink]._resume==FsmState::Resume);

	      sleep(1); // Do resume stuff
      
	      runWriterThreadFsm[slink]._command=FsmState::EndOfCommandEnum;
	      assert(runWriterThreadFsm[slink]._state==FsmState::Running);
    
	    }
	    runWriterThreadFsm[slink]._command=FsmState::EndOfCommandEnum;
	  }


	  if (runWriterThreadFsm[slink]._command!=FsmState::Stop &&
	      runWriterThreadFsm[slink]._command!=FsmState::Reset) {
	    std::cout << "FsmBase::runWriterThread while: slink = " << slink << " sleeping" <<  std::endl;
	    sleep(1); // Do running stuff
	  }

	  if(runWriterThreadFsm[slink]._command!=FsmState::Stop) {
	    sleep(3); // Do stop stuff
      
	    runWriterThreadFsm[slink]._state=FsmState::Configured;

	    std::cout << "FsmBase::runWriterThread  done: slink = " << slink <<  std::endl;

	  } else {
	    sleep(3); // Do reset stuff
      
	    runWriterThreadFsm[slink]._state=FsmState::Initial;

	    std::cout << "FsmBase::runWriterThread  done: slink = " << slink <<  std::endl;      
	  }
        
	  std::cout << "FsmBase::runWriterThread  stop: slink = " << slink << std::endl;
	}
      }
      //#endif 
   }
#include "exampleboard/swatch/NamespaceEnd.hpp"

#include "CommandTemplate.h"

#include "exampleboard/swatch/NamespaceStart.hpp"
   namespace commands {

     typedef CommandTemplate<FsmBase,FsmState,FsmState::Initialize> CommandDataTakingFsmInitialize;
     typedef CommandTemplate<FsmBase,FsmState,FsmState::Configure>  CommandDataTakingFsmConfigure;
     typedef CommandTemplate<FsmBase,FsmState,FsmState::Start>      CommandDataTakingFsmStart;
     typedef CommandTemplate<FsmBase,FsmState,FsmState::Pause>      CommandDataTakingFsmPause;
     typedef CommandTemplate<FsmBase,FsmState,FsmState::Resume>     CommandDataTakingFsmResume;
     typedef CommandTemplate<FsmBase,FsmState,FsmState::Stop>       CommandDataTakingFsmStop;
     typedef CommandTemplate<FsmBase,FsmState,FsmState::Halt>       CommandDataTakingFsmHalt;
     typedef CommandTemplate<FsmBase,FsmState,FsmState::Reset>      CommandDataTakingFsmReset;
     typedef CommandTemplate<FsmBase,FsmState,FsmState::End>        CommandDataTakingFsmEnd;
   }
//  }
//}
#include "exampleboard/swatch/NamespaceEnd.hpp"

#endif
