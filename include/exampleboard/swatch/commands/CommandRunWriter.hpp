#ifndef Hgcal10gLinkReceiver_CommandRunWriter_h
#define Hgcal10gLinkReceiver_CommandRunWriter_h

#include <iostream>
#include <iomanip>
#include <cstdint>
#include <cstring>

#include <yaml-cpp/yaml.h>

#include "DataFifo.h"
#include "FileWriter.h"

#include "../NamespaceStart.hpp"
namespace commands {

  typedef Hgcal10gLinkReceiver::DataFifoT<4,8*1024> RunWriterDataFifo;
  
  class CommandRunWriter {

  public:
    CommandRunWriter(unsigned l, RunWriterDataFifo *p) {
      _linkNumber=l;
      _dummyReader=false;
      _ptrFifoShm=p;
    }

    virtual ~CommandRunWriter() {
    }

    void setDummyReader(bool b) {
      _dummyReader=b;
    }
    
    bool initialize() {
      return true;
    }
    
    bool configure() {
      /*
      RecordConfiguring &r((RecordConfiguring&)(_ptrFsmInterface->record()));

      YAML::Node nRsa(YAML::Load(r.string()));
      _relayNumber=nRsa["RelayNumber"].as<uint32_t>();

      if(_relayNumber<0xffffffff) {
	std::ostringstream sout;
	sout << "dat/Relay" << std::setfill('0')
	     << std::setw(10) << _relayNumber;
      _fileWriter.setDirectory(sout.str());
      }
      */
      return true;
    }

    bool reconfigure() {
      return true;
    }

    bool start() {
      /*
      std::cout << "Starting flushing FIFO..." << std::endl;
      _ptrFifoShm->flush();
      std::cout << "Flushing complete" << std::endl;

      _eventNumber=0;
	
      _ptrFifoShm->starting();

      assert(_ptrFsmInterface->record().state()==FsmState::Starting);

      const RecordStarting *ry((const RecordStarting*)&(_ptrFsmInterface->record()));
      if(_printEnable) ry->print();
      YAML::Node nRsa(YAML::Load(ry->string()));
      _runNumber=nRsa["RunNumber"].as<uint32_t>();

      RecordStarting r;
      std::cout << "HERE0 starting" << std::endl;
      r.deepCopy(_ptrFsmInterface->record());
      r.print();
      _fileWriter.openRun(_runNumber,_linkNumber);
      _fileWriter.write(&r);
      */
      return true;
    }
    
    bool pause() {
      /*	
      RecordPrinter(&(_ptrFsmInterface->record()));
      RecordPausing r;
      std::cout << "HERE2 pausing" << std::endl;
      r.deepCopy(_ptrFsmInterface->record());
      r.print();
      //_fileWriter.write(&r); NO!
      */
      return true;
    }
    
    bool resume() {
      /*
      assert(_ptrFsmInterface->record().state()==FsmState::Resuming);
      RecordResuming r;
      std::cout << "HERE2 starting" << std::endl;
      r.deepCopy(_ptrFsmInterface->record());
      r.print();
      //_fileWriter.write(&r); NO!
      */
      return true;
    }
    
    bool stop() {
      /*
      assert(_ptrFsmInterface->record().state()==FsmState::Stopping);
      RecordStopping r;
      std::cout << "HERE2 stopping" << std::endl;
      r.deepCopy(_ptrFsmInterface->record());
      r.setNumberOfEvents(_eventNumber);
      r.print();
      _fileWriter.write(&r);
      _fileWriter.close();

      _ptrFifoShm->print();
      */
      return true;
    }    

    bool halt() {
      /*
      _ptrFifoShm->initialize();
      _ptrFifoShm->print();
      */
      return true;
    }    

    bool end() {
      return true;
    }    

    bool reset() {
      /*
      _ptrFifoShm->print();
      RecordResetting r;
      if(_printEnable) r.print();
      _fileWriter.write(&r);
      _fileWriter.close();
      _ptrFifoShm->print();
      */
      return true;
    }

    void running() {
      /*
      const Record *rrr;

      _ptrFsmInterface->setProcessState(FsmState::Running);
      
      while(_ptrFsmInterface->systemState()==FsmState::Running) {
	
	if(_ptrFifoShm->readable()==0) {
	  if(_dummyReader) _ptrFifoShm->writeIncrement();
	  usleep(10);

	} else {
	  rrr=_ptrFifoShm->readRecord();
	  _fileWriter.write(rrr);
	  _ptrFifoShm->readIncrement();
	  if(_dummyReader) _ptrFifoShm->writeIncrement();
	  _eventNumber++;
	}
      }

      std::cout << std::endl << "Finished loop, checking for other events, currently "
		<< _eventNumber << std::endl;
      _ptrFsmInterface->print();
      _ptrFifoShm->print();

      usleep(10000);

      unsigned nLoop(0);
      while(_ptrFifoShm->readable()>0) {
	while(_ptrFifoShm->readable()>0) {
	  rrr=_ptrFifoShm->readRecord();

	  _fileWriter.write(rrr);

	  _ptrFifoShm->readIncrement();
	  _eventNumber++;
	}

	nLoop++;
	_ptrFifoShm->print();
	usleep(1000);
      }

      std::cout << std::endl << "Finished loop " << nLoop
		<< ", no more checking for other events, currently "
		<< _eventNumber << std::endl;
      _ptrFifoShm->print();
*/
    }
   
  private:
    RunWriterDataFifo *_ptrFifoShm;
    FileWriter _fileWriter;
    unsigned _linkNumber;
    unsigned _eventNumber;
    unsigned _runNumber;
    unsigned _relayNumber;

    bool _dummyReader;
  };

}
#include "../NamespaceEnd.hpp"

#endif
