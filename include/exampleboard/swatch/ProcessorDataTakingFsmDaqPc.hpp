#ifndef __EXAMPLEBOARD_SWATCH_PROCESSORDATATAKINGFSMDAQPC_HPP__
#define __EXAMPLEBOARD_SWATCH_PROCESSORDATATAKINGFSMDAQPC_HPP__

#include "swatch/phase2/Processor.hpp"

#include "NamespaceStart.hpp"

class ProcessorDataTakingFsmDaqPc : public ::swatch::phase2::Processor {
public:
  ProcessorDataTakingFsmDaqPc(const ::swatch::core::AbstractStub& aStub);

  ~ProcessorDataTakingFsmDaqPc();

private:
  void retrievePropertyValues() final;

  void retrieveMetricValues() final;

  // FIXME: Add references to metrics (i.e. SimpleMetric<TYPE>&) representing any FPGA-level
  //        monitoring data that doesn't fit neatly into I/O, TCDS or Readout classes
};

#include "NamespaceEnd.hpp"

#endif
