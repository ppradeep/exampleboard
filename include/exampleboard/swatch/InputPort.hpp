#ifndef __EXAMPLEBOARD_SWATCH_INPUTPORT_HPP__
#define __EXAMPLEBOARD_SWATCH_INPUTPORT_HPP__

#include "swatch/phase2/InputPort.hpp"


namespace exampleboard {
namespace swatch {

// Represents an MGT RX port (and associated firmware implementing link protocol)
class InputPort : public ::swatch::phase2::InputPort {
public:
  InputPort(const size_t, const ::swatch::phase2::ChannelID&);

  ~InputPort();

private:
  static std::string createId(const size_t);

  bool checkPresence() const final;

  void retrievePropertyValues() final;

  void retrieveMetricValues() final;

  // FIXME: Add references to metrics (i.e. SimpleMetric<TYPE>&)
  //        representing your monitoring data for MGT RX ports
};

} // namespace swatch
} // namespace exampleboard


#endif