#ifndef __EXAMPLEBOARD_SWATCH_SERVICEMODULE_HPP__
#define __EXAMPLEBOARD_SWATCH_SERVICEMODULE_HPP__

#include "swatch/phase2/ServiceModule.hpp"


namespace exampleboard {
namespace swatch {

// Represents FPGAs and other components that form board services - e.g. Zynq device and/or other utility FPGAs (like Artix)
class ServiceModule : public ::swatch::phase2::ServiceModule {
public:
  ServiceModule(const ::swatch::core::AbstractStub&);
  ~ServiceModule();

private:
  void retrieveMetricValues() final;
};

} // namespace swatch
} // namespace exampleboard


#endif
