#ifndef __EXAMPLEBOARD_SWATCH_FIREFLYTX_HPP__
#define __EXAMPLEBOARD_SWATCH_FIREFLYTX_HPP__

#include "swatch/phase2/OpticalModule.hpp"


namespace exampleboard {
namespace swatch {

// Represents a 12-channel TX Firefly part
class FireflyTx : public ::swatch::phase2::OpticalModule {
public:
  FireflyTx(const std::string&);
  ~FireflyTx();

private:
  bool checkPresence() const final;

  void retrievePropertyValues() final;

  void retrieveMetricValues() final;

  void retrieveOutputMetricValues(Channel& aChannel) final;

  SimpleMetric<float>& mMetricTemperature;

  std::unordered_map<const Channel*, SimpleMetric<bool>*> mMetricLaserFault;

  // FIXME: Add references to metrics (i.e. SimpleMetric<TYPE>&)
  //        representing other monitoring data for this optical module
};


} // namespace swatch
} // namespace exampleboard


#endif