#ifndef __EXAMPLEBOARD_SWATCH_FIREFLYRX_HPP__
#define __EXAMPLEBOARD_SWATCH_FIREFLYRX_HPP__

#include "swatch/phase2/OpticalModule.hpp"


namespace exampleboard {
namespace swatch {

// Represents a 12-channel RX Firefly part
class FireflyRx : public ::swatch::phase2::OpticalModule {
public:
  FireflyRx(const std::string&);
  ~FireflyRx();

private:
  bool checkPresence() const final;

  void retrievePropertyValues() final;

  void retrieveMetricValues() final;

  void retrieveInputMetricValues(Channel& aChannel) final;

  SimpleMetric<float>& mMetricTemperature;

  // Optical power measured in each channel
  std::unordered_map<const Channel*, SimpleMetric<float>*> mMetricRxPower;

  // FIXME: Add references to metrics (i.e. SimpleMetric<TYPE>&)
  //        representing other monitoring data for this optical module
};


} // namespace swatch
} // namespace exampleboard


#endif